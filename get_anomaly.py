#
# get_anomaly.py Newton Raphson iteration to solve Kepler Equation for 
# eccentric anomaly # c.f. Spherical Astronomy W.M. Smart p 114
#    Copyright (C) 2019 A. Young, alison.young@leicester.ac.uk
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>

#/usr/bin/python
import numpy as np

tau = 2453859.6 # time of periapse passage (MJD)
t = 2456674.8 # time we want position for (MJD)
e = 0.379 # eccentricity
T = 4309.67705  # period (d)
days2sec = 86400.
au2m = 1.49598e11
a = 8.89 # semi major axis (au)
tolerance = 1e-5

#mean anomaly
M  = 2.* np.pi * (t-tau)/T # days2sec factor cancels
print "mean anomaly = ", M * 180./np.pi, "deg"

EccAnom = M
DeltaEccAnom = 0.

while DeltaEccAnom > tolerance :
	M0 = EccAnom - e * np.sin(EccAnom)
	DeltaEccAnom = (M - M0)/(1. - e*np.cos(EccAnom))
	EccAnom = EccAnom + DeltaEccAnom 

print "Eccentric anomaly =", EccAnom *180./np.pi, "deg"

true_anomaly = 2.* np.arctan( ((1. + e)/(1. - e))**(0.5) * np.tan( EccAnom /2.) )

if true_anomaly < 0. :
	true_anomaly = true_anomaly + 2. * np.pi
print "True anomaly =", true_anomaly,"radians", true_anomaly*180./np.pi, "deg"

# Checks:
#r1 = a *(1. - e**2)/(1. + e*np.cos(true_anomaly))
#r2 = a * (1. - e*np.cos(EccAnom))
#print "Check: Does r1 equal r2?"
#print "r1 = ", r1, "r2 = ", r2



