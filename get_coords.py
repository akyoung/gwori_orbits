#
# 	get_coords.py
# 	Code to calculate cartesian position and velocity vectors from orbital elements.
# 	True anomaly of C should be obtained by running get_anomaly.py first.
#
#    Copyright (C) 2019 A. Young, alison.young@leicester.ac.uk
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>

#/usr/bin/python
import numpy as np 

au2m = 1.49598e11
sphvunits = 2.9799e4 # velocities calculated for sphNG units - make sure this is consistent with hydro model

def getseparation(elements) :
	r = elements['a']*(1. - elements['e']**2.)/(1.+ elements['e']*np.cos(elements['trueanomaly']))
	#print "getseparation", elements['a'], elements['trueanomaly'], r
	if r < 0:
		print "error r<0: r=", r
		exit(1)
	return r

def comdistance(elements, M1, M2) :
	''' distance of body 1 from mutual centre of mass '''
	r = getseparation(elements) * M2/ (M1 + M2)
	return r

def position2D(r,theta) :
	''' [r,theta] to [x,y] coordinates '''
	vector = np.array([ r * np.cos(theta), r * np.sin(theta), 0.])
	return vector

def rotate(vector,elements) :
	''' apply 3D rotation matrix '''
	omega = -(elements['Omega'] - np.pi)
	i = -elements['i']
	w = -(elements['w'] - np.pi)
	newvector = np.zeros(3)
	rmatrix = np.zeros((3,3))
	rmatrix[0][0] = np.cos(w)*np.cos(omega) - np.cos(i)*np.sin(w)*np.sin(omega)
	rmatrix[0][1] = np.cos(omega)*np.sin(w) + np.cos(i)*np.cos(w)*np.sin(omega)
	rmatrix[0][2] = np.sin(omega)*np.sin(i)
	rmatrix[1][0] = -np.sin(omega)*np.cos(w) - np.cos(i)*np.sin(w)*np.cos(omega)
	rmatrix[1][1] = -np.sin(w)*np.sin(omega) + np.cos(i)*np.cos(omega)*np.cos(w)
	rmatrix[1][2] = np.cos(omega)*np.sin(i)
	rmatrix[2][0] = np.sin(i)*np.sin(w)
	rmatrix[2][1] = -np.sin(i)*np.cos(w)
	rmatrix[2][2] = np.cos(i)

	newvector[0] = -(rmatrix[0][0]*vector[0] + rmatrix[0][1]*vector[1] + rmatrix[0][2]*vector[2])
	newvector[1] = -(rmatrix[1][0]*vector[0] + rmatrix[1][1]*vector[1] + rmatrix[1][2]*vector[2])
	newvector[2] = -(rmatrix[2][0]*vector[0] + rmatrix[2][1]*vector[1] + rmatrix[2][2]*vector[2])
	return newvector

def v_tang(elements,m1,m2) :
	'''orbital speed (tangential velocity) of m1'''
	G = 6.67408e-11
	a = elements['a'] * au2m
	mu = G *(m1+m2) *1.9891e30
	r =  a*(1. - elements['e']**2.)/(1.+ elements['e']*np.cos(elements['trueanomaly']))
	v = np.sqrt(mu * ( (2./r) - (1./a) ) )
	print "a =", a, "r =", r,"v =", v
	return v

def flightangle(elements) :
	'''flight path angle. +ve is away from periapse'''
	e = elements['e']
	theta = elements['trueanomaly']
	angle = np.arctan(e * np.sin(theta) / (1 + e * np.cos(theta) ))
	if angle < 0 :
		angle = angle + 2.*np.pi
	return angle

def velocity2D(elements,m1,m2) :
	''' velocity of M1 in orbital plane relative to com '''
	v = v_tang(elements,m1,m2) # velocity
	trueanomaly = elements['trueanomaly']
	fangle = flightangle(elements)
	vector = np.array([v * np.sin(fangle - trueanomaly), v * np.cos(fangle - trueanomaly), 0.])
	velocity = vector / (1.+ m1/m2) 
	return velocity

# Orbital elements of stars - enter these here, see Kraus+(2020) for details
B = {
				'e' : 0.069, # eccentricity
				'a' : 1.2, # semi-major axis (au)
				'Omega' : 258.2 * np.pi/180. , # ascending node 
				'w' : 181.0 * np.pi/180. ,# periapsis
				'i' : 156.0 * np.pi/180., # inclination
				'trueanomaly' : 0.0 * np.pi/180. , #set to 0 as we start with B&A at periapse
				'period' : 241.619 # days
				}

C = {
				'e' : 0.379,
				'a' : 8.89, # au
				'Omega' : 230.9 * np.pi/180. ,
				'w' : 285.0 * np.pi/180. ,# periapsis
				'i' : 149.6 * np.pi/180. ,
				'trueanomaly' : 218.64 * np.pi/180. , #obtained by running get_anomaly.py
				'period' : 4216.8 # days
				}

Amass = 2.47 # Msun
Bmass = 1.43 # Msun
Cmass = 1.36 # Msun

# Name of files to write position and velocity data to
outfileprefix = "test"

# offset periapse by 180 deg. Note that the ascending nodes are at the same Omega
# to conserve momentum in the z direction. i.e. when body B 'descends', body A ascends
# "+ np.pi" is not necessary if \omega is defined in the visual binary conventions

AB = C.copy()
AB['w'] = AB['w'] + np.pi 
A = B.copy()
A['w'] = A['w'] + np.pi

print "C semi major axis", C['a'], "separation:", getseparation(C)

# Find C position relative to AB-C centre of mass (i.e. relative to (0,0,0))
# N.B. True anomalies are identical because ascending node offset by pi

Cposition = position2D(comdistance(C,Cmass,Amass+Bmass), C['trueanomaly'])
ABposition = position2D(comdistance(C,Amass+Bmass,Cmass), C['trueanomaly'])
print Cposition, ABposition

# Find A and B positions relative to A-B centre of mass
Bposition = position2D(comdistance(B,Bmass,Amass), B['trueanomaly'])
Aposition = position2D(comdistance(A, Amass,Bmass), A['trueanomaly'])

print "A and B relative to AB com:", Aposition, Bposition
print "A and B sky", rotate(Aposition,A), rotate(Bposition,B)

# Now rotate into correct sky positions 
CpositionSKY = rotate(Cposition,C)
ABpositionSKY = rotate(ABposition,AB)
BpositionSKY = rotate(Bposition,B) + ABpositionSKY
ApositionSKY = rotate(Aposition,A) + ABpositionSKY

# Write coords of AB com on sky
np.savetxt(outfileprefix+"ABcom.dat", ABpositionSKY)
# Write file of position vectors in au
outpositions = np.array([ApositionSKY,BpositionSKY,CpositionSKY])
np.savetxt(outfileprefix+"positions.dat", outpositions)

# Calculate the velocities
Cvelocity = velocity2D(C,Cmass,Amass+Bmass)
ABvelocity = velocity2D(AB, Amass+Bmass, Cmass)
Bvelocity = velocity2D(B, Bmass,Amass)
Avelocity = velocity2D(A, Amass, Bmass)
print "A 2d velocity", Avelocity/np.array([sphvunits,sphvunits,sphvunits])
print "B 2d velocity", Bvelocity/np.array([sphvunits,sphvunits,sphvunits])

# rotate to sky positions
CvelocitySKY = rotate(Cvelocity,C)
ABvelocitySKY = rotate(ABvelocity,AB)
BvelocitySKY = rotate(Bvelocity,B) + ABvelocitySKY
AvelocitySKY = rotate(Avelocity,A) + ABvelocitySKY

# Print for info
print "A xyz velocity", AvelocitySKY/sphvunits
print "B xyz velocity", BvelocitySKY/sphvunits
print "C vel", v_tang(C,Cmass,Amass+Bmass),np.linalg.norm(Cvelocity), np.linalg.norm(CvelocitySKY)
print "AB vel", v_tang(AB,Amass + Bmass, Cmass),np.linalg.norm(ABvelocity),np.linalg.norm(ABvelocitySKY)
print "B vel", v_tang(B,Bmass,Amass), np.linalg.norm(Bvelocity), np.linalg.norm(BvelocitySKY- ABvelocitySKY)
print "A vel", v_tang(A,Amass,Bmass), np.linalg.norm(Avelocity), np.linalg.norm(AvelocitySKY-ABvelocitySKY)
print "A angle", flightangle(A)
print "B angle", flightangle(B)
print "C angle", flightangle(C)
print "AB angle", flightangle(AB)

# Write file of velocity values in sphNG units
outvels = np.array([AvelocitySKY/sphvunits, BvelocitySKY/sphvunits, CvelocitySKY/sphvunits])
np.savetxt(outfileprefix+"velocities.dat", outvels)
print "Coordinate files written :)"
